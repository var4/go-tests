package main

import (
	"errors"
	"fmt"
	"log"
)

func volume(r float64) (float64, error) {
	if r < 0 {
		return 0, errors.New("Negative radius")
	}
	return (4.0 / 3.0) * 3.14 * r * r * r, nil
}

func errorHandles() {
	// version 1
	// f, err := os.Open("myFile.txt")

	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// defer f.Close()
	// fmt.Println("Fille successfully opened, ", f.Name())

	// check the type of errors
	// if _, err := os.Open("myFile.txt"); err != nil {
	// 	if errors.Is(err, os.ErrNotExist) {
	// 		log.Println("same type") // we wil get this statement
	// 	} else {
	// 		log.Println(err)
	// 	}
	// 	return
	// }
	// fmt.Println("success open file")

	// custom errors
	radius := -1.0
	vol, err := volume(radius)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Volume of a sphere is %0.2f", vol)
}
