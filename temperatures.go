package main

import "fmt"

func kelvinToCelcius(k float64) float64 {
	return k - 273.15
}

func celsiusToFarenheit(c float64) float64 {
	return (c * 9.0 / 5.0) + 32.0
}

func kelvinToFarenheit(k float64) float64 {
	return celsiusToFarenheit(kelvinToCelcius(k))
}

func compareDegree() {
	kelvin := 0.0
	celsius := kelvinToCelcius(kelvin)
	// farehheit := celsiusToFarenheit(celsius)
	farenheit2 := kelvinToFarenheit(kelvin)
	fmt.Println("kelvin: ", kelvin, "celsius ", celsius, "fareinheit ", farenheit2)
}
