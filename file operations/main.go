package main

// // Create file
// func main() {
// 	f, err := os.Create("create.txt")
// 	defer f.Close()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println(f) //&{0xc00007a780} (file pointer returns)
// }

// // Open and custom Close to see some comments
// func closer(f *os.File) error {
// 	f.Close()
// 	fmt.Println(f.Name(), "successfully closed")
// 	return nil
// }

// func main() {
// 	f, err := os.Open("create.txt")
// 	defer closer(f)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println(f.Name(), "successfully opened")
// }

// // Remove file
// func main() {
// 	err := os.Remove("create.txt") // check if file was already created
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("file successfully removed")
// }

// //Copy file
// func main() {
// 	src, err := os.Open("src.txt")
// 	defer src.Close()
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	dst, err := os.OpenFile("dst.txt", os.O_RDWR|os.O_CREATE, 0755)
// 	defer dst.Close()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	value, err := io.Copy(dst, src)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println(reflect.TypeOf(value))
// 	fmt.Println(value)
// }

// // Rename or move a file
// func main() {
// 	oldPath := "file.txt"
// 	newPath := "./new/newFile.txt"
// 	err := os.Rename(oldPath, newPath)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("file successfully moved and renamed")
// }
