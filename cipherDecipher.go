package main

import (
	"fmt"
)

// decipher
func mainDecipher() {
	cipherText := "CSOITEUIWUIZNSROCNKFD" // WEDIGYOULUVTHEGOPHERS
	// cipherText := "ECFRZKYGLGRNKFP" // yourmessagehere
	keyword := "GOLANG"
	message := ""
	keyIndex := 0
	for i := 0; i < len(cipherText); i++ {
		// 'A' is 65
		letter := cipherText[i] - 'A'
		keyLetter := keyword[keyIndex] - 'A'
		fmt.Println("keyLetter ", keyLetter) // 6

		// There are 26 letters in eng alphabet
		letter = (letter-keyLetter+26)%26 + 'A' //87
		message += string(letter)

		fmt.Println("letter ", letter)

		keyIndex++
		fmt.Println("keyIndex", keyIndex)

		// check if we reach last letter of keyword
		keyIndex %= len(keyword)
		fmt.Println("keyIndex", keyIndex)
	}
	fmt.Println("message", message)
}

// cipher
// func main() {
// 	plainString := "your message here"
// 	keyword := "GOLANG"
// 	formattedString := strings.ToUpper(strings.Replace(plainString, " ", "", -1))

// 	cipherText := ""
// 	keyIndex := 0

// 	for i := 0; i < len(formattedString); i++ {
// 		letter := formattedString[i]
// 		if letter >= 'A' && letter <= 'Z' {
// 			letter -= 'A'
// 			keyLetter := keyword[keyIndex] - 'A'
// 			letter = (letter+keyLetter)%26 + 'A'

// 			keyIndex++
// 			keyIndex %= len(keyword)
// 		}

// 		cipherText += string(letter)
// 	}
// 	fmt.Println(cipherText)
// }
