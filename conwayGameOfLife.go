// Conway’s Game of Life

package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	width  = 10
	height = 10
)

// two dimentional field of cells
type Universe [][]bool

// create new empty and dead universe (false)
func NewUniverse() Universe {
	universe := make(Universe, height)
	for i := range universe {
		universe[i] = make([]bool, width)
	}
	return universe
}

// seed random 25% of field by alive cells (true)
func (u Universe) Seed() {
	quater := width * height / 4
	for i := 0; i < quater; i++ {
		// first param - heigh, second - width
		u.Set(rand.Intn(height), rand.Intn(width), true)
	}
}

// set of condition of a cell
func (u Universe) Set(x, y int, condition bool) {
	u[x][y] = condition
}

// return the condition of cell - isCellAlive
func (u Universe) IsAlive(x, y int) bool {
	x = (x + height) % height
	y = (y + width) % width
	return u[x][y]
}

// calc neighbor alive cells (-1; 1)
func (u Universe) Neighbors(x, y int) int {
	neighbors := 0
	for v := -1; v <= 1; v++ {
		for h := -1; h <= 1; h++ {
			if !(v == 0 && h == 0) && u.IsAlive(x+v, y+h) {
				neighbors++
			}
		}
	}
	return neighbors
}

// returns condition of following (next) cell
func (u Universe) Next(x, y int) bool {
	n := u.Neighbors(x, y)
	return n == 3 || n == 2 && u.IsAlive(x, y)
}

// return universe as a string
func (u Universe) String() string {
	var b byte
	buf := make([]byte, 0, (width+1)*height)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			b = ' '
			if u[y][x] {
				b = '*'
			}
			buf = append(buf, b)
		}
		buf = append(buf, '\n')
	}

	return string(buf)
}

// log of universe
func (u Universe) Show() {
	fmt.Print("\x0c", u.String())
}

func Step(a, b Universe) {
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			b.Set(x, y, a.Next(x, y))
		}
	}
}

func main() {
	a, b := NewUniverse(), NewUniverse()
	a.Seed()

	for i := 0; i < 20; i++ {
		Step(a, b)
		a.Show()
		time.Sleep(time.Second / 30)
		a, b = b, a // Swap universes
	}

	// counter := 0
	// for i := 0; i < height; i++ {
	// 	for j := 0; j < width; j++ {
	// 		if uni[i][j] {
	// 			counter++
	// 		}
	// 	}
	// }
	// fmt.Println(counter)
	// fmt.Println(uni)
}
