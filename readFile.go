package main

import (
	"fmt"
	"log"
	"os"
)

func readingFile() {
	contents, err := os.ReadFile("file.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(contents))
}
