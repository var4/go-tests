package main

import (
	"fmt"
	"math/rand"
)

func selecTicket() {
	const secondsPerDay = 86400
	spacelines := [3]string{"Space Adventures", "SpaceX", "Virgin Galactic"}
	tripTypes := [2]string{"Round trip", "One-way trip"}
	tripType := tripTypes[rand.Intn(2)]

	distance := 62100000
	speed := rand.Intn(15) + 16
	spaceLine := spacelines[rand.Intn(3)]
	duration := distance / speed / secondsPerDay
	priceOneWay := 36 + speed
	var totalPrice int

	if tripType == "Round trip" {
		totalPrice = priceOneWay * 2
	} else {
		totalPrice = priceOneWay
	}

	fmt.Println("The company ", spaceLine, " will deliver you to Mars in ", duration, " days with speed ", speed, "km/h. The ", tripType, " will cost ", totalPrice, " mil $. Have a good trip!")
}

// The company  Virgin Galactic  will deliver you to Mars in  25  days with speed  28 km/h. The  One-way trip  will cost  64  mil $. Have a good trip!
