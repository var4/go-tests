package main

// // MkdirAll to create directory
// func main() {
// 	dir, err := os.Stat("subdir")
// 	fmt.Println("error returned from os.Stat() is ", err)
// 	if err == nil {
// 		fmt.Println(dir.Mode(), dir.IsDir())
// 		log.Fatal("subdir already exist")
// 	}

// 	if errors.Is(err, os.ErrNotExist) {
// 		// func MkdirAll (pass string, perm FileMode) error
// 		err := os.MkdirAll("subdir", 0777) // 0777 - all rights
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		fmt.Println("subdir successfully created")
// 	}
// }

// // MksirAll for nested directories
// func main() {
// 	path := filepath.Join("../test", "subdir1", "subdir2")
// 	err := os.MkdirAll(path, 0777)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println(path, " nested directory created")
// }
